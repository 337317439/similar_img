#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint similar_img.podspec` to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'similar_img'
  s.version          = '0.0.1'
  s.summary          = 'A new Flutter project.'
  s.description      = <<-DESC
A new Flutter project.
                       DESC
  s.homepage         = 'http://example.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Your Company' => 'email@example.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.dependency 'Flutter'
  s.platform = :ios, '10.0'
  
  s.dependency 'OpenCV2', '~> 4.3.0'
  s.dependency 'YYModel'
  s.dependency 'JSONModel'
  s.libraries = ["z", "c++"]
  s.frameworks = ["UIKit", "Foundation", "CoreGraphics", "Photos"]
  s.static_framework = true

  # Flutter.framework does not contain a i386 slice.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
  s.swift_version = '5.0'
end
