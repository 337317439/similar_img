//
//  PMAssetEntity.h
//  similar_img
//
//  Created by osulcode.xiao on 2022/3/31.
//

#import <Foundation/Foundation.h>
#import <Photos/PHAsset.h>
#import <Photos/Photos.h>
#import "PMThumbLoadOption.h"
#import <Flutter/Flutter.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^ImageBlock)(__nullable id);

@interface PMAssetEntity : NSObject

@property(nonatomic, copy) NSString *id;
@property(nonatomic, assign) long createDt;
@property(nonatomic, assign) NSUInteger width;
@property(nonatomic, assign) NSUInteger height;
@property(nonatomic, assign) long duration;
@property(nonatomic, assign) int type;
@property(nonatomic, strong) PHAsset *phAsset;
@property(nonatomic, assign) long modifiedDt;
@property(nonatomic, assign) double lat;
@property(nonatomic, assign) double lng;
@property(nonatomic, copy) NSString *title;
@property(nonatomic, assign) int subtype;
@property(nonatomic, assign) BOOL favorite;
@property(nonatomic, assign) BOOL isLocallyAvailable;

- (instancetype)initWithId:(NSString *)assetId
                  createDt:(long)createDt
                     width:(NSUInteger)width
                    height:(NSUInteger)height
                  duration:(long)duration
                      type:(int)type;

+ (instancetype)entityWithId:(NSString *)assetId
                    createDt:(long)createDt
                       width:(NSUInteger)width
                      height:(NSUInteger)height
                    duration:(long)duration
                        type:(int)type;

+ (PMAssetEntity *)convertPHAssetToAssetEntity:(PHAsset *)asset
                                     needTitle:(BOOL)needTitle;

+ (void)getThumbWithId:(NSString *)assetId option:(PMThumbLoadOption *)option block:(ImageBlock)block;

+ (void)getOriginImageWithId:(NSString *)assetId block:(ImageBlock)block;

+ (void)removeAssetWithId:(NSString *)assetId type:(int)type block:(ImageBlock)block;

@end

NS_ASSUME_NONNULL_END
