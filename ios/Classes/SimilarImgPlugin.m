#import "SimilarImgPlugin.h"
#if __has_include(<similar_img/similar_img-Swift.h>)
#import <similar_img/similar_img-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "similar_img-Swift.h"
#endif

@implementation SimilarImgPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftSimilarImgPlugin registerWithRegistrar:registrar];
}
@end
