import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'entity.dart';
import 'similar_img_platform_interface.dart';

/// An implementation of [SimilarImgPlatform] that uses method channels.
class MethodChannelSimilarImg extends SimilarImgPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('similar_img');

  @override
  Future<String> getPlatformVersion() async {
    final version =
        await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }

  @override
  Future<List<List<AssetEntity>>> fectchSimilarArray() async {
    final list = await methodChannel.invokeMethod<List>('fectchSimilarArray');
    List<List<AssetEntity>> listP = [];
    for (var element in list) {
      List<AssetEntity> listC = [];
      for (var child in element) {
        var tmp = child as Map;
        listC.add(AssetEntity.fromJson(tmp));
      }
      listP.add(listC);
    }
    return listP;
  }

  @override
  Future<Uint8List> getThumb(Map map) async {
    final data = await methodChannel.invokeMethod<Uint8List>('getThumb', map);
    return data;
  }

  @override
  Future<String> getOrigin(String id) async {
    final data = await methodChannel.invokeMethod<String>('getOrigin', id);
    return data;
  }

  @override
  Future<String> removeAsset(String id, int type) async {
    final data = await methodChannel
        .invokeMethod<String>('removeAsset', {"id": id, "type": type});
    return data;
  }
}
