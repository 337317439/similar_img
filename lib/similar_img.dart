import 'dart:typed_data';
import 'entity.dart';
import 'similar_img_platform_interface.dart';

class SimilarImg {
  Future<String> getPlatformVersion() {
    return SimilarImgPlatform.instance.getPlatformVersion();
  }

  Future<List<List<AssetEntity>>> fectchSimilarArray() {
    return SimilarImgPlatform.instance.fectchSimilarArray();
  }

  Future<Uint8List> getThumb(Map map) {
    return SimilarImgPlatform.instance.getThumb(map);
  }

  Future<String> getOrigin(String id) {
    return SimilarImgPlatform.instance.getOrigin(id);
  }

  Future<String> removeAeest(String id, int type) {
    return SimilarImgPlatform.instance.removeAsset(id, type);
  }
}
