import 'dart:typed_data';

import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'entity.dart';
import 'similar_img_method_channel.dart';

abstract class SimilarImgPlatform extends PlatformInterface {
  /// Constructs a SimilarImgPlatform.
  SimilarImgPlatform() : super(token: _token);

  static final Object _token = Object();

  static SimilarImgPlatform _instance = MethodChannelSimilarImg();

  /// The default instance of [SimilarImgPlatform] to use.
  ///
  /// Defaults to [MethodChannelSimilarImg].
  static SimilarImgPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [SimilarImgPlatform] when
  /// they register themselves.
  static set instance(SimilarImgPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }

  Future<List<List<AssetEntity>>> fectchSimilarArray() {
    throw UnimplementedError('fectchSimilarArray() has not been implemented.');
  }

  Future<Uint8List> getThumb(Map map) {
    throw UnimplementedError('getThumb() has not been implemented.');
  }

  Future<String> getOrigin(String id) {
    throw UnimplementedError('getOrigin() has not been implemented.');
  }

  Future<String> removeAsset(String id, int type) {
    throw UnimplementedError('removeAsset() has not been implemented.');
  }
}
