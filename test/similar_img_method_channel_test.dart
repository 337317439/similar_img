import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:similar_img/similar_img_method_channel.dart';

void main() {
  MethodChannelSimilarImg platform = MethodChannelSimilarImg();
  const MethodChannel channel = MethodChannel('similar_img');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await platform.getPlatformVersion(), '42');
  });
}
