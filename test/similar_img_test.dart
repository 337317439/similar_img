import 'dart:typed_data';
import 'package:flutter_test/flutter_test.dart';
import 'package:similar_img/entity.dart';
import 'package:similar_img/similar_img.dart';
import 'package:similar_img/similar_img_platform_interface.dart';
import 'package:similar_img/similar_img_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockSimilarImgPlatform
    with MockPlatformInterfaceMixin
    implements SimilarImgPlatform {
  @override
  Future<String> getPlatformVersion() => Future.value('42');

  @override
  Future<List<List<AssetEntity>>> fectchSimilarArray() {
    throw UnimplementedError();
  }

  @override
  Future<Uint8List> getThumb(Map map) {
    throw UnimplementedError();
  }

  @override
  Future<String> getOrigin(String id) {
    throw UnimplementedError();
  }

  @override
  Future<String> removeAsset(String id, int type) {
    throw UnimplementedError();
  }
}

void main() {
  final SimilarImgPlatform initialPlatform = SimilarImgPlatform.instance;

  test('$MethodChannelSimilarImg is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelSimilarImg>());
  });

  test('getPlatformVersion', () async {
    SimilarImg similarImgPlugin = SimilarImg();
    MockSimilarImgPlatform fakePlatform = MockSimilarImgPlatform();
    SimilarImgPlatform.instance = fakePlatform;

    expect(await similarImgPlugin.getPlatformVersion(), '42');
  });
}
